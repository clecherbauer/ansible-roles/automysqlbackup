automysqlbackup
==========

Ansible playbook that installs automysqlbackup on Debian

Role Variables
--------------

No variables are defined so far.

automysqlbackup is installed with the following defaults:

| - | Path on system |
|-----|-----|
|Daily cron: | `/etc/cron.daily/automysqlbackup` |
|Configuration: | `/etc/default/automysqlbackup` |


Example Playbook
----------------

The following will install automysqlbackup on your local host.

```yaml
# file: test.yml
- hosts: local
        
  roles:
    - automysqlbackup
```